## Pelagic Ecosystem Classification

![SANBI logo](images/sanbi_logo.jpg)

This GitLab group was created to house, version control and, ultimately, share the workflows contributing to the pelagic ecosystem classification that underpins the pelagic layers of South Africa's Marine Ecosystem Map.
